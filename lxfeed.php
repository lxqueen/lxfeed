<?php defined( 'ABSPATH' ) or die( '<h1>Forbidden</h1>' );

/*
 * Plugin Name: LxFeed
 * Description: A lightweight RSS to post importer for WordPress.
 * Version:     0.1.0
 * Author:      Alexis Queen
 * Author URI:  http://lexi.parateam.uk
 * Text Domain: lxfeed
 */
